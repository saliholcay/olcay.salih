#Euler Number Calculation
This project goal was to develope to finde the smallest number that can be divided by sequntial set of numbers. 
For Example:
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
360360 is the smallest number that can be divided by each of the numbers from 1 to 15 without any remainder.

#Geting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
See deployment for notes on how to deploy the project on a live system.

###Installing
  - Install Eclipse Oxygen with JEE ability
  - Install Apache Tomcat Server version 9.0
  - Install Maven plugin for Eclipse m2e integration
  - Install Spring plugin for Eclipse
  - Install Git and Git Plugin for Eclipse
  - Import maven project ot Eclipse from BitBucket
  - Update Maven dependecies
  - Deploy project ot Tomcat Server.
  - PostMan
 
 ##Runing the tests
 Test are done using JUnit and Mockito frameworks. You can run tests using JUnit in Eclipse.
 
 ##Deployment
 You can deploy the project to TomCat server using generated war file in Eclipse.
 
   
   
   

 
      