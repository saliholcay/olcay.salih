package com.euler.numbers.rest;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.swing.text.NumberFormatter;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.euler.numbers.entity.EulerJSON;
import com.euler.numbers.entity.EulerXML;

@RestController
@RequestMapping(value = "/api")
public class CalculateAndReportSmalestController {

	static Logger logger = Logger.getLogger(CalculateAndReportSmalestController.class.getName());

	private EulerXML theEulerX;
	private EulerJSON theEulerJ;

	/*
	 * Calculate Euler for given number
	 */
	private long calculateEuler(long maxNumber) {
		long ans = 1;
		for (int i = 1; i <= maxNumber; i++) {
			ans = (ans * i) / (egcd(ans, i));
		}
		return ans;
	}

	/*
	 * Finding greatest common devisor
	 */
	private long egcd(long a, long b) {
		if (a == 0) {
			return b;
		}
		while (b != 0) {
			if (a > b) {
				a = a - b;
			} else {
				b = b - a;
			}
		}
		return a;
	}

	/*
	 * Formating time seconds/milliseconds
	 */
	private String millisecondToSecondFormater(long milSec) {
		NumberFormat formater = new DecimalFormat("#0.00000");
		return formater.format(milSec / 1000d);
	}

	/*
	 * Creating POJO objects
	 */
	@PostConstruct
	public void setTheEuler() {
		theEulerX = new EulerXML();
		theEulerJ = new EulerJSON();
	}

	/*
	 * Calculating SCD 
	 * Output is writen in XML
	 */
	@GetMapping("/xml/eulers/{eNumber}")
	public EulerXML getEulerX(@RequestParam("eNumber") long eNumber) {
		logger.info(">>getEulerX Begin for " + eNumber + ">>");
		long start = System.currentTimeMillis();
		logger.info(">> getEulerX Calling calculate Euler>>");
		theEulerX.setResult(calculateEuler(eNumber));
		long end = System.currentTimeMillis();
		logger.info(">>getEulerX Calculating elapsed time>>");
		theEulerX.setElapsedTime(millisecondToSecondFormater(end - start) + " seconds");
		logger.info(">>getEulerX is done>>");
		return theEulerX;

	}

	/*
	 * Calculating SCD 
	 * Output is writen in JSON
	 */
	@GetMapping("/json/eulers/{eNumber}")
	public EulerJSON getEulerJ(@RequestParam("eNumber") long eNumber) {
		logger.info(">>getEuler Begin for " + eNumber + ">>");
		long start = System.currentTimeMillis();
		logger.info(">>getEulerJ Calling calculate Euler>>");
		theEulerJ.setResult(calculateEuler(eNumber));
		long end = System.currentTimeMillis();
		logger.info(">>getEulerJ Calculating elapsed time>>");
		theEulerJ.setElapsedTime(millisecondToSecondFormater(end - start) + " seconds");
		logger.info(">>getEulerJ is done>>");
		return theEulerJ;

	}
}
