package com.euler.numbers.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class EulerNumberExceptionHandler {
	@ExceptionHandler
	public ResponseEntity<EulerNumbersErrorResponse> handleExcepton(Exception exc) {
		EulerNumbersErrorResponse error = new EulerNumbersErrorResponse();
	    
	    error.setStatus(HttpStatus.BAD_REQUEST.value());
	    error.setMessage(exc.getMessage());
	    error.setTimeStamp(System.currentTimeMillis());
		
		return new ResponseEntity<>(error,HttpStatus.BAD_REQUEST);	
	}
		

}
