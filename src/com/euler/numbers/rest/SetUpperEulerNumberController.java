package com.euler.numbers.rest;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.euler.numbers.entity.EulerJSON;
import com.euler.numbers.entity.EulerXML;

@RestController
public class SetUpperEulerNumberController {

    static Logger logger = Logger.getLogger(SetUpperEulerNumberController.class.getName());
    
    /*
     * Dependencie Injection of calculation controller
     */
    @Autowired
    private CalculateAndReportSmalestController calculateAndReportSmalestController;
	
    /*
     * Call injected controller to calculate SCD for given number
     * Report output as XML
     */
    @RequestMapping(value = "/api/eulerx/{eNumber}",produces="application/xml")
	public EulerXML getEulerNumberX(@PathVariable int eNumber) {
		if (eNumber < 1 || eNumber > 25) {
			  logger.info(">>Number must be between 1 to 25: " + eNumber +">>" );
			  throw new EulerNumberBadRequestException("Number must be between 1 to 25: " + eNumber );  
		}
		return calculateAndReportSmalestController.getEulerX(eNumber);
		
	}

    /*
     * Call injected controller to calculate SCD for given number
     * Report output as JSON
     */
    @RequestMapping(value = "/api/eulerj/{eNumber}",produces="application/json")
	public EulerJSON getEulerNumberJ(@PathVariable int eNumber) {
		if (eNumber < 1 || eNumber > 25) {
   		    logger.info(">>Number must be between 1 to 25: " + eNumber +">>" );
			throw new EulerNumberBadRequestException("Number must be between 1 to 25: " + eNumber );  
		}
		return calculateAndReportSmalestController.getEulerJ(eNumber);
		
	}
	

}
