package com.euler.numbers.rest;


public class EulerNumberBadRequestException extends RuntimeException {
	public EulerNumberBadRequestException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public EulerNumberBadRequestException(String arg0) {
		super(arg0);
	}

	public EulerNumberBadRequestException(Throwable arg0) {
		super(arg0);
	}
	 
}
