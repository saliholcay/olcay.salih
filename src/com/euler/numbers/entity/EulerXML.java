package com.euler.numbers.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EulerXML {
	@XmlElement
	private long result;
	@XmlElement
	private String elapsedTime;
	
	
	public EulerXML() {
	}
	
	public EulerXML(long result, String elapsedTime) {
		this.result = result;
		this.elapsedTime = elapsedTime;
	}


	public long getResult() {
		return result;
	}
	public void setResult(long result) {
		this.result = result;
	}
	public String getElapsedTime() {
		return elapsedTime;
	}
	public void setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
	
	
	
}
