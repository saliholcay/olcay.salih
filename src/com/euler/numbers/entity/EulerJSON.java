package com.euler.numbers.entity;


public class EulerJSON {
	private long result;
	private String elapsedTime;
	
	
	public EulerJSON() {
	}
	
	public EulerJSON(long result, String elapsedTime) {
		this.result = result;
		this.elapsedTime = elapsedTime;
	}


	public long getResult() {
		return result;
	}
	public void setResult(long result) {
		this.result = result;
	}
	public String getElapsedTime() {
		return elapsedTime;
	}
	public void setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
	
	
	
}
