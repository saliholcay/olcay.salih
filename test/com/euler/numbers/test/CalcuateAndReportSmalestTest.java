package com.euler.numbers.test;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.euler.numbers.rest.CalculateAndReportSmalestController;

public class CalcuateAndReportSmalestTest {
	
	@InjectMocks
	private CalculateAndReportSmalestController calculateAndReportSmalestController;

	private MockMvc mockMVC;
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMVC = MockMvcBuilders.standaloneSetup(calculateAndReportSmalestController).build();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetEuler() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testCalculateEuler() throws Exception {
		this.mockMVC.perform(get("//json/eulers/10")).andExpect(status().isOk());
	}

}
